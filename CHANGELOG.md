# Changelog


## 2.0.0

- must be manually built, push with a version equal to pico-sdk
- starts following latest release in https://github.com/raspberrypi/pico-sdk

## 1.5.1

- pegged to pico sdk v1.5.1


## 1.4.0

- pegged to pico sdk v1.4.0

## 1.2.0

### Added

- https://github.com/illusiaDecafish/bootselBoot.git