FROM debian:bookworm-20240513-slim

ARG sdkversion
ENV sdkver $sdkversion

RUN apt update && apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi build-essential python3 python3-requests git curl wget libbsd-dev python3-pip -y &&\
    rm -rf /var/lib/apt/lists/* &&\
    git clone https://github.com/raspberrypi/pico-sdk.git --branch $sdkver &&\
    cd pico-sdk &&\
    git submodule update --init &&\
    apt-get -yqq autoremove && apt-get -yqq autoclean && \
    rm -rf /var/lib/apt/lists/* /tmp/*

ENV PICO_SDK_PATH=/pico-sdk

RUN echo "Pico SDK Version: $(cat /pico-sdk/pico_sdk_version.cmake | grep "set(PICO_SDK_VERSION_" | grep -v "{" | grep -v "PRE_RELEASE_ID" | awk -F' ' '{print $2}' | tr '\n' '.' | sed 's/\.$//' | sed 's/)//g')"
